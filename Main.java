

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
	
	private static int code=0;
	private static int space=0;
	private static int filenum=1;//根目录原本就有1
	public static void main(String[] args) throws IOException {
		try {
			Path path = Paths.get(args[0]);
//			File file=new File(args[0]);//获得参数，需要打包成jar
//			File file=new File("d://test");//测试用
			queryjava(path.toFile());
			System.out.println("代码总行数："+code);
			System.out.println("空白总行数："+space);
			System.out.println("文件总数："+filenum);
	
		} catch (Exception e) {
			System.out.println("请输入合法路径！");
		}		
	}

	public static void queryjava(File file) throws IOException {
		File[] file2 =file.listFiles();
		for (int i = 0; i < file2.length; i++) {
			
			if(file2[i].isDirectory()){
				filenum++;
				queryjava(file2[i]);
			}else if(file2[i].isFile()&&file2[i].getName().endsWith(".java")){
				calculate(file2[i]);
			}
		}
	}
	public static void calculate(File file) throws IOException {
		BufferedReader bufferedReader= new BufferedReader(new FileReader(file));
		String string="";
		while ((string=bufferedReader.readLine())!=null) {
			string = string.trim();
			if(string.length()==0){
				space++;
			}else{
				code++;
			}
		}
	}
}
